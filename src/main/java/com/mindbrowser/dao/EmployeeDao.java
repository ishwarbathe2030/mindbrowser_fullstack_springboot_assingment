package com.mindbrowser.dao;

import java.util.List;
import com.mindbrowser.bean.Employee;

public interface EmployeeDao {

	List<Employee> empList(Long managerId) throws Exception;

}
