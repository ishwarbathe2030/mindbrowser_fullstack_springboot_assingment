package com.mindbrowser.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.mindbrowser.constants.Filter;
import com.mindbrowser.bean.SubscriptionPackage;
import com.mindbrowser.bean.UserSubscription;
import com.mindbrowser.dao.SubscriptionDAO;
import com.mindbrowser.dao.SubscriptionPackageDAO;
import com.mindbrowser.setting.ConnectionDao;

/**
 * @author ISHWAR BATHE
 * @since DECEMBER 13, 2020
 * 
 */

@Repository(value = "subscriptionPackageDAO")
@Transactional
public class SubscriptionPackageDAOIMPL extends ConnectionDao implements SubscriptionPackageDAO {

	private final SubscriptionDAO subscriptionDAO;

	public SubscriptionPackageDAOIMPL(SubscriptionDAO subscriptionDAO) {
		super();
		this.subscriptionDAO = subscriptionDAO;
	}

	@Override
	public List<SubscriptionPackage> list() throws Exception {
		List<SubscriptionPackage> subscriptionPackageList = new ArrayList<SubscriptionPackage>();
		SubscriptionPackage subscriptionPackage = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM subscription_package sp WHERE sp.package_id IS NOT NULL ORDER BY sp.package_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				subscriptionPackage = new SubscriptionPackage();
				subscriptionPackage.setPackageId(rs.getLong("package_id"));
				subscriptionPackage.setAmount(rs.getBigDecimal("amount"));
				subscriptionPackage.setPackageName(rs.getString("package_name"));
				subscriptionPackage.setShortDescription(rs.getString("short_description"));
				subscriptionPackage.setStatus(rs.getBoolean("status"));
				subscriptionPackage.setValidityKey(rs.getInt("validity_key"));
				subscriptionPackage.setValidityValue(rs.getInt("validity_value"));
				subscriptionPackage.setLongDescription(rs.getString("long_description"));
				subscriptionPackageList.add(subscriptionPackage);
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return subscriptionPackageList;
	}

	@Override
	public List<SubscriptionPackage> listActive(Filter filter) throws Exception {
		List<SubscriptionPackage> subscriptionPackageList = new ArrayList<SubscriptionPackage>();
		SubscriptionPackage subscriptionPackage = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM subscription_package sp WHERE sp.package_id IS NOT NULL AND status = 1   ORDER BY sp.package_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				subscriptionPackage = new SubscriptionPackage();
				subscriptionPackage.setPackageId(rs.getLong("package_id"));
				subscriptionPackage.setAmount(rs.getBigDecimal("amount"));
				subscriptionPackage.setPackageName(rs.getString("package_name"));
				subscriptionPackage.setShortDescription(rs.getString("short_description"));
				subscriptionPackage.setStatus(rs.getBoolean("status"));
				subscriptionPackage.setValidityKey(rs.getInt("validity_key"));
				subscriptionPackage.setValidityValue(rs.getInt("validity_value"));
				subscriptionPackage.setLongDescription(rs.getString("long_description"));

				if (filter.getUserId() != null) {
					List<UserSubscription> uS = subscriptionDAO.packageOnGoingStatus(filter.getUserId(),
							subscriptionPackage.getPackageId());
					System.out.println(uS.size());
					if (uS.size() > 0)
						subscriptionPackage.setSubscribed(true);
					else
						subscriptionPackage.setSubscribed(false);
				}
				subscriptionPackageList.add(subscriptionPackage);
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return subscriptionPackageList;
	}

}
