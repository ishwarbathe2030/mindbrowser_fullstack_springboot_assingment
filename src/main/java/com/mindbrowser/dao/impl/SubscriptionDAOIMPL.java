/**
 * 
 */
package com.mindbrowser.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.mindbrowser.bean.User;
import com.mindbrowser.constants.Filter;
import com.mindbrowser.bean.UserSubscription;
import com.mindbrowser.dao.SubscriptionDAO;
import com.mindbrowser.setting.ConnectionDao;
import com.mindbrowser.setting.RepositoryDao;

/**
 * @author ISHWAR BATHE
 * @since DECEMBER 13, 2020
 * 
 */

@Repository(value = "subscriptionDAO")
@Transactional
public class SubscriptionDAOIMPL extends ConnectionDao implements SubscriptionDAO {

	private final RepositoryDao repositoryDao;

	public SubscriptionDAOIMPL(RepositoryDao repositoryDao) {
		super();
		this.repositoryDao = repositoryDao;
	}

	@Override
	public List<UserSubscription> list(Filter filter) throws Exception {
		List<UserSubscription> userSubscriptionList = new ArrayList<UserSubscription>();
		UserSubscription userSubscription = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * ,datediff(us.expiry_date, CURDATE()) as remaingDays FROM user_subscription us WHERE us.payment_status != 1 ";
			if (filter.getSubscription() != null && filter.getSubscription().equalsIgnoreCase("Ongoing")) {
				sql += "  AND datediff(us.expiry_date, CURDATE()) > 0";
			}
			if (filter.getSubscription() != null && filter.getSubscription().equalsIgnoreCase("Expired")) {
				sql += "  AND datediff(us.expiry_date, CURDATE()) <= 0";
			}
			sql += " ORDER BY us.purchase_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				userSubscription = new UserSubscription();
				userSubscription.setPurchaseId(rs.getLong("purchase_id"));
				userSubscription.setAmount(rs.getBigDecimal("amount"));
				userSubscription.setCreatedAt(rs.getDate("created_at"));
				userSubscription.setDevice(rs.getString("device"));
				userSubscription.setExpiryDate(rs.getDate("expiry_date"));
				userSubscription.setFinalPrice(rs.getBigDecimal("final_price"));
				userSubscription.setLongDescription(rs.getString("long_description"));
				userSubscription.setPackageId(rs.getLong("package_id"));
				userSubscription.setPackageName(rs.getString("package_name"));
				userSubscription.setPurchageNo(rs.getString("purchase_no"));
				userSubscription.setPaymentStatus(rs.getInt("payment_status"));
				userSubscription.setRefralCode(rs.getString("refral_code"));
				userSubscription.setRefundReason(rs.getString("refund_reason"));
				userSubscription.setShortDescription(rs.getString("short_description"));
				userSubscription.setTransactionId(rs.getString("transaction_id"));
				userSubscription.setUserId(rs.getLong("user_id"));
				userSubscription.setValidityKey(rs.getInt("validity_key"));
				userSubscription.setValidityValue(rs.getInt("validity_value"));
				userSubscription.setRemaingDays(rs.getInt("remaingDays"));
				User user = repositoryDao.findById(User.class, userSubscription.getUserId());
				userSubscription.setUser(user);
				userSubscriptionList.add(userSubscription);
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return userSubscriptionList;
	}

	@Override
	public List<UserSubscription> userSubscriptionList(Long id) throws Exception {
		List<UserSubscription> userSubscriptionList = new ArrayList<UserSubscription>();
		UserSubscription userSubscription = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * ,datediff(us.expiry_date, CURDATE()) as remaingDays  FROM user_subscription us WHERE us.payment_status != 1 AND user_id = ?   ORDER BY us.purchase_id ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				userSubscription = new UserSubscription();
				userSubscription.setPurchaseId(rs.getLong("purchase_id"));
				userSubscription.setAmount(rs.getBigDecimal("amount"));
				userSubscription.setCreatedAt(rs.getDate("created_at"));
				userSubscription.setDevice(rs.getString("device"));
				userSubscription.setExpiryDate(rs.getDate("expiry_date"));
				userSubscription.setFinalPrice(rs.getBigDecimal("final_price"));
				userSubscription.setLongDescription(rs.getString("long_description"));
				userSubscription.setPackageId(rs.getLong("package_id"));
				userSubscription.setPackageName(rs.getString("package_name"));
				userSubscription.setPurchageNo(rs.getString("purchase_no"));
				userSubscription.setPaymentStatus(rs.getInt("payment_status"));
				userSubscription.setRefralCode(rs.getString("refral_code"));
				userSubscription.setRefundReason(rs.getString("refund_reason"));
				userSubscription.setShortDescription(rs.getString("short_description"));
				userSubscription.setTransactionId(rs.getString("transaction_id"));
				userSubscription.setUserId(rs.getLong("user_id"));
				userSubscription.setValidityKey(rs.getInt("validity_key"));
				userSubscription.setValidityValue(rs.getInt("validity_value"));
				User user = repositoryDao.findById(User.class, userSubscription.getUserId());
				userSubscription.setUser(user);
				userSubscription.setRemaingDays(rs.getInt("remaingDays"));
				userSubscriptionList.add(userSubscription);
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return userSubscriptionList;
	}

	@Override
	public UserSubscription subscription(Long id) throws Exception {
		UserSubscription userSubscription = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM user_subscription us WHERE  purchase_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				userSubscription = new UserSubscription();
				userSubscription.setPurchaseId(rs.getLong("purchase_id"));
				userSubscription.setAmount(rs.getBigDecimal("amount"));
				userSubscription.setCreatedAt(rs.getDate("created_at"));
				userSubscription.setDevice(rs.getString("device"));
				userSubscription.setExpiryDate(rs.getDate("expiry_date"));
				userSubscription.setFinalPrice(rs.getBigDecimal("final_price"));
				userSubscription.setLongDescription(rs.getString("long_description"));
				userSubscription.setPackageId(rs.getLong("package_id"));
				userSubscription.setPackageName(rs.getString("package_name"));
				userSubscription.setPurchageNo(rs.getString("purchase_no"));
				userSubscription.setPaymentStatus(rs.getInt("payment_status"));
				userSubscription.setRefralCode(rs.getString("refral_code"));
				userSubscription.setRefundReason(rs.getString("refund_reason"));
				userSubscription.setShortDescription(rs.getString("short_description"));
				userSubscription.setTransactionId(rs.getString("transaction_id"));
				userSubscription.setUserId(rs.getLong("user_id"));
				userSubscription.setValidityKey(rs.getInt("validity_key"));
				userSubscription.setValidityValue(rs.getInt("validity_value"));
				User user = repositoryDao.findById(User.class, userSubscription.getUserId());
				userSubscription.setUser(user);
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return userSubscription;
	}

	@Override
	public List<UserSubscription> packageOnGoingStatus(Long userId, Long packageId) throws Exception {
		List<UserSubscription> userSubscriptionList = new ArrayList<UserSubscription>();
		UserSubscription userSubscription = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT datediff(us.expiry_date, CURDATE()) as remaingDays  FROM user_subscription us "
					+ " WHERE us.payment_status != 1 AND user_id = ?  AND us.package_id = ?  ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, packageId);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				userSubscription = new UserSubscription();
				if (rs.getInt("remaingDays") > 0) {
					userSubscription.setRemaingDays(rs.getInt("remaingDays"));
					userSubscriptionList.add(userSubscription);
				}
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return userSubscriptionList;
	}

	@Override
	public boolean isPackageOnGoing(Long userId) throws Exception {
		Connection conn = null;
		boolean isPakcageOnGoing = false;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT datediff(us.expiry_date, CURDATE()) as remaingDays  FROM user_subscription us "
					+ " WHERE us.payment_status != 1 AND user_id = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, userId);
			System.out.println("IS PACAKGE ONGOING" + ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getInt("remaingDays") > 0) {
					isPakcageOnGoing = true;
				}
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return isPakcageOnGoing;
	}

}
