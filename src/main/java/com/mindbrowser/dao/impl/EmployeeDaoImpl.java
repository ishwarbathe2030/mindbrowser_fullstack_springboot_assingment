package com.mindbrowser.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.mindbrowser.bean.Address;
import com.mindbrowser.bean.Employee;
import com.mindbrowser.dao.EmployeeDao;
import com.mindbrowser.setting.ConnectionDao;
import com.mindbrowser.setting.RepositoryDao;

@Repository(value = "EmployeeDao")
@Transactional
public class EmployeeDaoImpl extends ConnectionDao implements EmployeeDao {

	private final RepositoryDao repositoryDao;

	public EmployeeDaoImpl(RepositoryDao repositoryDao) {
		super();
		this.repositoryDao = repositoryDao;
	}

	@Override
	public List<Employee> empList(Long managerId) throws Exception {
		List<Employee> empList = new ArrayList<Employee>();
		Connection conn = null;
		Employee employee = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM employee  WHERE manager_id = ? ORDER BY emp_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, managerId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				employee = new Employee();
				employee.setEmpId(rs.getLong("emp_id"));
				employee.setDob(rs.getDate("dob"));
				employee.setFirstName(rs.getString("first_name"));
				employee.setLastName(rs.getString("last_name"));
				employee.setMobile(rs.getString("mobile"));
				Address address = repositoryDao.findById(Address.class, rs.getLong("adddress_id"));
				employee.setAddress(address);
				empList.add(employee);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return empList;
	}

}
