/**
 * 
 */
package com.mindbrowser.dao;

import java.util.List;

import com.mindbrowser.constants.Filter;
import com.mindbrowser.bean.UserSubscription;

/**
 * @author ISHWAR BATHE
 * @since DECEMBER 13, 2020
 * 
 */

public interface SubscriptionDAO {

	List<UserSubscription> list(Filter filter) throws Exception;

	List<UserSubscription> userSubscriptionList(Long id) throws Exception;
	
	List<UserSubscription>  packageOnGoingStatus(Long userId,Long packageId) throws Exception;
	
	UserSubscription subscription(Long id) throws Exception;
	
	boolean isPackageOnGoing(Long userId) throws Exception;

}
