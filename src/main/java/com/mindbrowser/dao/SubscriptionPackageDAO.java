package com.mindbrowser.dao;

import java.util.List;

import com.mindbrowser.constants.Filter;
import com.mindbrowser.bean.SubscriptionPackage;

public interface SubscriptionPackageDAO {
	
	public List<SubscriptionPackage> list() throws Exception;
	
	public List<SubscriptionPackage> listActive(Filter filter) throws Exception;

}
