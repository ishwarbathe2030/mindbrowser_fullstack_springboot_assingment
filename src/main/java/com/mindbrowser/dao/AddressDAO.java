package com.mindbrowser.dao;

import java.util.List;

import com.mindbrowser.cc.AddressCC;

public interface AddressDAO {
	
	public List<AddressCC> userAddressList(Long userId) throws Exception;

}
