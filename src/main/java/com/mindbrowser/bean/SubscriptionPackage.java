package com.mindbrowser.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;


@Entity
@Audited
@Table(name = "subscription_package")
public class SubscriptionPackage implements Serializable {

	private static final long serialVersionUID = 8103794597947450640L;

	@Id
	@Column(name = "package_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long packageId;

	@Column(name = "package_name", nullable = false)
	private String packageName;

	@Column(name = "amount", columnDefinition = "DECIMAL(10,2) DEFAULT 0.00", nullable = false)
	private BigDecimal amount;

	@Column(name = "short_description")
	private String shortDescription;
	
	@Column(name = "long_description", columnDefinition = "LONGTEXT DEFAULT NULL")
	private String longDescription;

	
	/****
	 * 1 For Day
	 * 2 For Month
	 * 3 For Year 
	 * 
	 */
	@Column(name = "validity_key", nullable = false)
	private int validityKey;

	/**
	 * Add No
	 * Ex validityValue + Validity Key
	 * 20 Days
	 */
	@Column(name = "validity_value", nullable = false)
	private int validityValue;

	@Column(name = "status", columnDefinition = "boolean default true")
	private boolean status;

	
	@Column(name = "created_at", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "updated_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date updatedAt;
	
	@Transient
	private boolean isSubscribed;
	
	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}



	public int getValidityKey() {
		return validityKey;
	}

	public void setValidityKey(int validityKey) {
		this.validityKey = validityKey;
	}

	public int getValidityValue() {
		return validityValue;
	}

	public void setValidityValue(int validityValue) {
		this.validityValue = validityValue;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isSubscribed() {
		return isSubscribed;
	}

	public void setSubscribed(boolean isSubscribed) {
		this.isSubscribed = isSubscribed;
	}

	
	
}
