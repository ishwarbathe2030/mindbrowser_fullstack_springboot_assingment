package com.mindbrowser.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mindbrowser.bean.User;

@Entity
@Table(name = "user_subscription")
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class UserSubscription implements Serializable {

	private static final long serialVersionUID = 6680777090031093803L;

	@Id
	@Column(name = "purchase_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long purchaseId;

	@Column(name = "amount", columnDefinition = "DECIMAL(10,2) DEFAULT 0.00", nullable = false)
	private BigDecimal amount;

	@Column(name = "final_price", nullable = false, columnDefinition = "Decimal(10,2) default 0.00")
	private BigDecimal finalPrice = new BigDecimal(0);

	@Column(name = "purchase_no", length = 20)
	private String purchageNo;

	@Column(name = "refral_code")
	private String refralCode;

	@Column(name = "package_id")
	private Long packageId;

	@Column(name = "package_name", nullable = false)
	private String packageName;

	@Column(name = "short_description")
	private String shortDescription;

	@Column(name = "long_description", columnDefinition = "LONGTEXT DEFAULT NULL")
	private String longDescription;

	/****
	 * 1 For Day 
	 * 2 For Month 
	 * 3 For Year
	 * 
	 */
	@Column(name = "validity_key", nullable = false)
	private int validityKey;

	/**
	 * Add No 
	 * Ex validityValue + Validity Key 20 Days
	 */
	@Column(name = "validity_value", nullable = false)
	private int validityValue;

	/***
	 * 1 For PENDING 
	 * 2 For ON_GOING 
	 * 3 for CANCELLED
	 *********/
	@Column(name = "payment_status", nullable = true, columnDefinition = "INTEGER DEFAULT 1")
	private Integer paymentStatus;

	@Column(name = "refund_reason", columnDefinition = "LONGTEXT DEFAULT NULL")
	private String refundReason;

	/*
	 * GET FROM PAYMENT GATWAY
	 * 
	 */
	@Column(name = "transaction_id", columnDefinition = "LONGTEXT DEFAULT NULL")
	private String transactionId;

	/**
	 * Transaction From
	 * 1 Android
	 * 2 IOS
	 */
	@Column(name = "device", nullable = true, length = 25)
	private String device;

	@Column(name = "created_at", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "updated_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date updatedAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expiry_date", nullable = false)
	private Date expiryDate;

	@OneToOne(targetEntity = User.class, cascade = CascadeType.DETACH)
	@JoinColumn(name = "user_id", nullable = true)
	private User user;

	@Column(name = "cancel_date", nullable = true)
	private Date cancelDate;

	@Transient
	private Long userId;

	@Transient
	private String expiryDateStr;

	@Transient
	private int remaingDays;

	@Transient
	private String createdDateStr;

	public Long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Long purchaseId) {
		this.purchaseId = purchaseId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getPurchageNo() {
		return purchageNo;
	}

	public void setPurchageNo(String purchageNo) {
		this.purchageNo = purchageNo;
	}

	public String getRefralCode() {
		return refralCode;
	}

	public void setRefralCode(String refralCode) {
		this.refralCode = refralCode;
	}

	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getValidityKey() {
		return validityKey;
	}

	public void setValidityKey(int validityKey) {
		this.validityKey = validityKey;
	}

	public int getValidityValue() {
		return validityValue;
	}

	public void setValidityValue(int validityValue) {
		this.validityValue = validityValue;
	}

	public String getExpiryDateStr() {
		return expiryDateStr;
	}

	public void setExpiryDateStr(String expiryDateStr) {
		this.expiryDateStr = expiryDateStr;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public Integer getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Integer paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public int getRemaingDays() {
		return remaingDays;
	}

	public void setRemaingDays(int remaingDays) {
		this.remaingDays = remaingDays;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}
	
	

}
