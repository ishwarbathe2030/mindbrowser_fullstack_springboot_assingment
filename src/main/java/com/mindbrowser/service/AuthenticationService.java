/**
 * 
 */
package com.mindbrowser.service;

import com.mindbrowser.cc.Authentication;
import com.mindbrowser.constants.Response;

public interface AuthenticationService {

	public Response loginWithPass(Authentication authentication) throws Exception;

	public Response signUp(Authentication authentication) throws Exception;

}
