
package com.mindbrowser.service;
import com.mindbrowser.constants.Response;
import com.mindbrowser.bean.UserSubscription;
import com.mindbrowser.cc.PaymentGateway;
import com.mindbrowser.constants.Filter;

public interface SubscriptionService {

	public Response addnew(UserSubscription userSubscription) throws Exception;

	public Response update(PaymentGateway paymentGateway) throws Exception;
	
	public Response paymentSuccessfull(Long id) throws Exception;

	public Response cancelSubscription(Long subscriptionId) throws Exception;
	
	public Response resumeSubscription(Long subscriptionId) throws Exception;

	public Response list(Filter filter) throws Exception;

	public Response userSubscriptionList(Long id) throws Exception;

}
