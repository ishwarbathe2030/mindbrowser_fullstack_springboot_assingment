package com.mindbrowser.service;

import com.mindbrowser.bean.Employee;
import com.mindbrowser.constants.Response;

public interface EmployeeService {

	Response empList(Long managerId) throws Exception;

	Response addnew(Employee emp) throws Exception;
	
	Response update(Employee emp) throws Exception;

	Response delete(Long empId) throws Exception;

}
