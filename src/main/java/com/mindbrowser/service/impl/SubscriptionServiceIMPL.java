package com.mindbrowser.service.impl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.mindbrowser.bean.User;
import com.mindbrowser.cc.PaymentGateway;
import com.mindbrowser.constants.Filter;
import com.mindbrowser.constants.Response;
import com.mindbrowser.constants.StatusConstance;
import com.mindbrowser.bean.UserSubscription;
import com.mindbrowser.dao.SubscriptionDAO;
import com.mindbrowser.service.SubscriptionService;
import com.mindbrowser.setting.RepositoryDao;
import com.mindbrowser.util.RandomGenerator;

@Service
public class SubscriptionServiceIMPL implements SubscriptionService {

	private final RepositoryDao repositoryDao;

	private final SubscriptionDAO subscriptionDAO;

	private final Environment environment;

	public SubscriptionServiceIMPL(RepositoryDao repositoryDao, SubscriptionDAO subscriptionDAO,
			Environment environment) {
		super();
		this.repositoryDao = repositoryDao;
		this.subscriptionDAO = subscriptionDAO;
		this.environment = environment;
	}

	@Override
	public Response addnew(UserSubscription userSubscription) throws Exception {
		Response response = new Response();
		Calendar cal = Calendar.getInstance();
		try {
			if (userSubscription.getUserId() != null) {
				User user = repositoryDao.findById(User.class, userSubscription.getUserId());
				userSubscription.setUser(user);
				userSubscription.setPurchageNo("SUB" + RandomGenerator.generateNum(5));

				if (userSubscription.getValidityKey() == 3) {
					cal.add(Calendar.YEAR, userSubscription.getValidityValue());
					Date nextYear = cal.getTime();
					userSubscription.setExpiryDate(nextYear);
				} else if (userSubscription.getValidityKey() == 2) {
					cal.add(Calendar.MONTH, userSubscription.getValidityValue());
					Date month = cal.getTime();
					userSubscription.setExpiryDate(month);
				} else if (userSubscription.getValidityKey() == 1) {
					cal.add(Calendar.DATE, userSubscription.getValidityValue());
					Date date = cal.getTime();
					userSubscription.setExpiryDate(date);
				}
				userSubscription.setFinalPrice(userSubscription.getAmount());
				userSubscription.setPaymentStatus(1);
				repositoryDao.addnew(userSubscription);
				response.setResponse(paymentGatewayReponse(userSubscription));
				response.setTitle("Subscription Saved");
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage("Redirecting to payment gateway");
			} else {
				response.setTitle("Oops...currently request not completed..Please try again");
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("User Id is required to complete process");
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response update(PaymentGateway paymentGateway) throws Exception {
		Response response = new Response();
		try {
			UserSubscription user = repositoryDao.findById(UserSubscription.class, paymentGateway.getOrderId());
			if (user != null) {
				user.setTransactionId(paymentGateway.getTransactionId());
				user.setPaymentStatus(2);
				repositoryDao.update(user);
				response.setResponse(user);
				response.setTitle("PAYMENT DONE SUCCESSFULLY");
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage("Subscription Purchased Successfully..");
			} else {
				response.setTitle("Subscription Not found");
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("By passed subscription  id. Subscription Detail not found.");
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	public PaymentGateway paymentGatewayReponse(UserSubscription userSubscription) {
		PaymentGateway ordersResponse = new PaymentGateway();

		ordersResponse.setAmount(userSubscription.getFinalPrice());
		String name = "";
		if (userSubscription.getUser().getFirstName() != null) {
			name += userSubscription.getUser().getFirstName() + " ";
		}
		if (userSubscription.getUser().getLastName() != null) {
			name += userSubscription.getUser().getLastName();
		}
		ordersResponse.setOrderId(userSubscription.getPurchaseId());
		if (userSubscription.getUser().getMobile() != null) {
			ordersResponse.setContact(userSubscription.getUser().getMobile());
		} else {
			ordersResponse.setContact(environment.getRequiredProperty("tempContact"));
		}
		if (userSubscription.getUser().getEmail() != null) {
			ordersResponse.setEmail(userSubscription.getUser().getEmail());
		} else {
			ordersResponse.setEmail(environment.getRequiredProperty("tempEmail"));
		}
		ordersResponse.setName(name);
		String address = "Pune";
		ordersResponse.setAddress(address);
		return ordersResponse;
	}

	@Override
	public Response paymentSuccessfull(Long id) throws Exception {
		Response response = new Response();
		try {
			UserSubscription user = repositoryDao.findById(UserSubscription.class, id);
			response.setResponse(user);
			response.setTitle("Subscription Purchased Successfully");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Subscription Detail");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response list(Filter filter) throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(subscriptionDAO.list(filter));
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response userSubscriptionList(Long id) throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(subscriptionDAO.userSubscriptionList(id));
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response cancelSubscription(Long subscriptionId) throws Exception {
		Response response = new Response();
		try {
			UserSubscription userSubscription = repositoryDao.findById(UserSubscription.class, subscriptionId);
			// Add Todays date as cancel date
			userSubscription.setCancelDate(new Date());
			userSubscription.setPaymentStatus(3);
			repositoryDao.update(userSubscription);
			response.setTitle("Subscription cancelled");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Subscription cancelled Successfully");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response resumeSubscription(Long subscriptionId) throws Exception {
		Response response = new Response();
		Calendar cal = Calendar.getInstance();
		try {
			UserSubscription userSubscription = repositoryDao.findById(UserSubscription.class, subscriptionId);

			// parse method is used to parse
			// the text from a string to
			// produce the date
			Date d1 = userSubscription.getCancelDate();
			Date d2 = new Date();

			// calculate time difference in milliseconds
			long difference_In_Time = d2.getTime() - d1.getTime();

			long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;

			System.out.println(difference_In_Days);
			
			//add cancel date and current date diffrance in expiry date
			
			cal.add(Calendar.DATE, (int) difference_In_Days);
			Date date = cal.getTime();
			userSubscription.setExpiryDate(date);
			userSubscription.setPaymentStatus(2);
			repositoryDao.update(userSubscription);
			response.setTitle("Subscription Resume SUccessfully");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Subscription Resume Successfully");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
