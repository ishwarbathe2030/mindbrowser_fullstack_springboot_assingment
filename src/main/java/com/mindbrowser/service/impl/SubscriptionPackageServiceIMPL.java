package com.mindbrowser.service.impl;
import org.springframework.stereotype.Service;

import com.mindbrowser.constants.Filter;
import com.mindbrowser.constants.Response;
import com.mindbrowser.constants.StatusConstance;
import com.mindbrowser.bean.SubscriptionPackage;
import com.mindbrowser.dao.SubscriptionPackageDAO;
import com.mindbrowser.service.SubscriptionPackageService;
import com.mindbrowser.setting.RepositoryDao;

@Service
public class SubscriptionPackageServiceIMPL implements SubscriptionPackageService {

	private final RepositoryDao repositoryDao;

	private final SubscriptionPackageDAO subscriptionPackageDAO;

	public SubscriptionPackageServiceIMPL(RepositoryDao repositoryDao, SubscriptionPackageDAO subscriptionPackageDAO) {
		super();
		this.repositoryDao = repositoryDao;
		this.subscriptionPackageDAO = subscriptionPackageDAO;
	}

	@Override
	public Response addnew(SubscriptionPackage subscriptionPackage) throws Exception {
		Response response = new Response();
		try {
			subscriptionPackage.setStatus(true);
			repositoryDao.addnew(subscriptionPackage);
			response.setTitle(StatusConstance.SUCCESS_ADD);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Exam Paper Added Successfully");
			response.setResponse(subscriptionPackage);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response update(SubscriptionPackage subscriptionPackage) throws Exception {
		Response response = new Response();
		try {

			if (subscriptionPackage.getPackageId() != null) {
				SubscriptionPackage pack = repositoryDao.findById(SubscriptionPackage.class,
						subscriptionPackage.getPackageId());
				if (pack != null) {
					pack.setAmount(subscriptionPackage.getAmount());
					pack.setPackageName(subscriptionPackage.getPackageName());
					pack.setShortDescription(subscriptionPackage.getShortDescription());
					pack.setStatus(subscriptionPackage.isStatus());
					pack.setValidityKey(subscriptionPackage.getValidityKey());
					pack.setValidityValue(subscriptionPackage.getValidityValue());
					pack.setLongDescription(subscriptionPackage.getLongDescription());
					repositoryDao.update(pack);
					response.setTitle(StatusConstance.SUCCESS_ADD);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage("Pakcage Updated Successfully");
					response.setResponse(pack);
				} else {
					response.setTitle(StatusConstance.DATA_NOT_FOUND);
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage("Pakcage Not found ");
				}
			} else {
				response.setTitle(StatusConstance.ID_REQ_TO_UPDATE);
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("Package Id Required to update");
			}

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response findById(Long id) throws Exception {
		Response response = new Response();
		try {
			if (id != null) {
				SubscriptionPackage subscriptionPackage = repositoryDao.findById(SubscriptionPackage.class, id);
				if (subscriptionPackage != null) {
					response.setTitle(StatusConstance.DATA_FOUND);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.RECORD_FOUND);
					response.setResponse(subscriptionPackage);
				} else {
					response.setTitle(StatusConstance.DATA_NOT_FOUND);
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage("Subscription Object Not found");
				}
			} else {
				response.setTitle(StatusConstance.ID_REQ);
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("Subscription" + StatusConstance.ID_REQ_TO_UPDATE);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response delete(Long id) throws Exception {
		Response response = new Response();
		try {
			if (id != null) {
				SubscriptionPackage subscriptionPackage = repositoryDao.findById(SubscriptionPackage.class, id);
				if (subscriptionPackage != null) {
					repositoryDao.deleteObj(subscriptionPackage);
					response.setTitle(StatusConstance.DELETE);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.DELETE_MSG);
				} else {
					response.setTitle(StatusConstance.DATA_NOT_FOUND);
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage("Subscription Object Not found");
				}
			} else {
				response.setTitle(StatusConstance.ID_REQ);
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("Subscription" + StatusConstance.ID_REQ_TO_UPDATE);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response changeStatus(Long id) throws Exception {
		Response response = new Response();
		try {
			if (id != null) {
				SubscriptionPackage subscriptionPackage = repositoryDao.findById(SubscriptionPackage.class, id);
				if (subscriptionPackage != null) {
					if (subscriptionPackage.isStatus())
						subscriptionPackage.setStatus(false);
					else
						subscriptionPackage.setStatus(true);
					repositoryDao.update(subscriptionPackage);
					response.setTitle(StatusConstance.STATUS_CHANGE);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.STATUS_CHANGE_MSG + subscriptionPackage.isStatus());
				} else {
					response.setTitle(StatusConstance.DATA_NOT_FOUND);
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage("Subscription Object Not found");
				}
			} else {
				response.setTitle(StatusConstance.ID_REQ);
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("Subscription" + StatusConstance.ID_REQ_TO_UPDATE);
			}

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response list() throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(subscriptionPackageDAO.list());
		} catch (Exception e) {
			throw e;
		}
		return response;
	}
	
	@Override
	public Response listActive(Filter filter) throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(subscriptionPackageDAO.listActive(filter));
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
