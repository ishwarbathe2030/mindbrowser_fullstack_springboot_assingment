
package com.mindbrowser.service.impl;

import org.springframework.stereotype.Service;

import com.mindbrowser.bean.Address;
import com.mindbrowser.bean.Employee;
import com.mindbrowser.bean.User;
import com.mindbrowser.constants.Response;
import com.mindbrowser.constants.StatusConstance;
import com.mindbrowser.dao.EmployeeDao;
import com.mindbrowser.service.EmployeeService;
import com.mindbrowser.setting.RepositoryDao;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeDao employeeDao;

	private final RepositoryDao repositoryDao;

	public EmployeeServiceImpl(EmployeeDao employeeDao, RepositoryDao repositoryDao) {
		super();
		this.employeeDao = employeeDao;
		this.repositoryDao = repositoryDao;
	}

	@Override
	public Response empList(Long managerId) throws Exception {
		Response response = new Response();
		try {
			response.setResponse(employeeDao.empList(managerId));
			response.setTitle("ALL USER LIST");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("All User List");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response addnew(Employee emp) throws Exception {
		Response response = new Response();
		try {
			if (emp.getManagerId() != null) {
				Address add = emp.getAddress();
				repositoryDao.addnew(add);
				emp.setAddress(add);
				User user = repositoryDao.findById(User.class, emp.getManagerId());
				emp.setManager(user);
				repositoryDao.addnew(emp);
				response.setTitle("RECORED UPADED");
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage("Employee recored added successfully.");
			} else {
				response.setTitle("Required Field Missing");
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("Manager id is misssing...Please add manager id in request to add new employee");
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response update(Employee emp) throws Exception {
		Response response = new Response();
		try {

			Employee emloyee = repositoryDao.findById(Employee.class, emp.getEmpId());

			if (emp.getAddress().getAddressId() != null) {
				Address address = repositoryDao.findById(Address.class, emp.getAddress().getAddressId());
				address.setArea(emp.getAddress().getArea());
				address.setCity(emp.getAddress().getCity());
				address.setCountry(emp.getAddress().getCountry());
				address.setLandmark(emp.getAddress().getLandmark());
				address.setLine1(emp.getAddress().getLine1());
				address.setLine2(emp.getAddress().getLine2());
				address.setPostalCode(emp.getAddress().getPostalCode());
				address.setState(emp.getAddress().getState());
				repositoryDao.update(address);
				emloyee.setAddress(address);
			}
			emloyee.setDob(emp.getDob());
			emloyee.setFirstName(emp.getFirstName());
			emloyee.setLastName(emp.getLastName());
			emloyee.setMobile(emp.getMobile());
			repositoryDao.update(emloyee);
			response.setTitle("RECORED UPADED");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Employee recored updated successfully.");

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response delete(Long userId) throws Exception {
		Response response = new Response();
		try {
			Employee emloyee = repositoryDao.findById(Employee.class, userId);
			repositoryDao.deleteObj(emloyee);
			response.setTitle("RECORED DELETED");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Employee recored deleted successfully.");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
