
package com.mindbrowser.service;
import com.mindbrowser.constants.Filter;
import com.mindbrowser.constants.Response;
import com.mindbrowser.bean.SubscriptionPackage;


public interface SubscriptionPackageService {

	public Response addnew(SubscriptionPackage subscriptionPackage) throws Exception;

	public Response update(SubscriptionPackage subscriptionPackage) throws Exception;

	public Response findById(Long id) throws Exception;

	public Response delete(Long id) throws Exception;

	public Response changeStatus(Long id) throws Exception;

	public Response list() throws Exception;
	
	public Response listActive(Filter filter) throws Exception;
}
