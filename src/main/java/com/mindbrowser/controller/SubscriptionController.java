/**
 * 
 */
package com.mindbrowser.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindbrowser.cc.PaymentGateway;
import com.mindbrowser.constants.Filter;
import com.mindbrowser.constants.Response;
import com.mindbrowser.constants.StatusConstance;
import com.mindbrowser.bean.UserSubscription;
import com.mindbrowser.service.SubscriptionService;

/**
 * @author ISHWAR BATHE
 * @since DECEMBER 13, 2020
 * 
 */

@RestController
@RequestMapping(value = "/v1/subscription")
public class SubscriptionController {

	private static final Logger logger = LogManager.getLogger(SubscriptionController.class);

	private final SubscriptionService subscriptionService;

	public SubscriptionController(SubscriptionService subscriptionService) {
		super();
		this.subscriptionService = subscriptionService;
	}

	@CrossOrigin
	@PostMapping("/add")
	public Response addnew(@RequestBody UserSubscription userSubscription) {
		Response response = new Response();
		try {
			return subscriptionService.addnew(userSubscription);
		} catch (Exception e) {
			logger.error("Error In add user subscription  : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@PutMapping("/update")
	public Response update(@RequestBody PaymentGateway paymentGateway) {
		Response response = new Response();
		try {
			return subscriptionService.update(paymentGateway);
		} catch (Exception e) {
			logger.error("Error In update subscription : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@GetMapping("/cancel-subscription/{subscriptionId}")
	public Response findById(@PathVariable("subscriptionId") Long subscriptionId) {
		Response response = new Response();
		try {
			return subscriptionService.cancelSubscription(subscriptionId);
		} catch (Exception e) {
			logger.error("Error in cancel  subscription   : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@GetMapping("/resume-subscription/{subscriptionId}")
	public Response resumeSubscription(@PathVariable("subscriptionId") Long subscriptionId) {
		Response response = new Response();
		try {
			return subscriptionService.resumeSubscription(subscriptionId);
		} catch (Exception e) {
			logger.error("Error in resume subscription   : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@PostMapping("/list")
	public Response list(@RequestBody Filter filter) {
		Response response = new Response();
		try {
			return subscriptionService.list(filter);
		} catch (Exception e) {
			logger.error("Error in subscription list ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@GetMapping("/user_subscription_list/{id}")
	public Response userSubscriptionList(@PathVariable("id") Long id) {
		Response response = new Response();
		try {
			return subscriptionService.userSubscriptionList(id);
		} catch (Exception e) {
			logger.error("Error in subscription list ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

}
