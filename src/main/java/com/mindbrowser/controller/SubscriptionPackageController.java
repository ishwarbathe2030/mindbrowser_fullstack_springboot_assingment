package com.mindbrowser.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindbrowser.constants.Filter;
import com.mindbrowser.constants.Response;
import com.mindbrowser.constants.StatusConstance;
import com.mindbrowser.bean.SubscriptionPackage;
import com.mindbrowser.service.SubscriptionPackageService;


@RestController
@RequestMapping(value = "/v1/subscription-package")
public class SubscriptionPackageController {

	private static final Logger logger = LogManager.getLogger(SubscriptionPackageController.class);

	private final SubscriptionPackageService subscriptionPackageService;

	public SubscriptionPackageController(SubscriptionPackageService subscriptionPackageService) {
		super();
		this.subscriptionPackageService = subscriptionPackageService;
	}

	/***
	 * ADD EXAM PAPER
	 */
	@CrossOrigin
	@PostMapping("/add")
	public Response addnew(@RequestBody SubscriptionPackage subscriptionPackage) {
		Response response = new Response();
		try {
			return subscriptionPackageService.addnew(subscriptionPackage);
		} catch (Exception e) {
			logger.error("Error In add subscription package : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	/***
	 * UPDATE EXAM PAPER
	 */
	@CrossOrigin
	@PutMapping("/update")
	public Response update(@RequestBody SubscriptionPackage subscriptionPackage) {
		Response response = new Response();
		try {
			return subscriptionPackageService.update(subscriptionPackage);
		} catch (Exception e) {
			logger.error("Error In update subscription package : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	/***
	 * GET EXAM PAPER DETAIL
	 */
	@CrossOrigin
	@GetMapping("/{id}")
	public Response findById(@PathVariable("id") Long id) {
		Response response = new Response();
		try {
			return subscriptionPackageService.findById(id);
		} catch (Exception e) {
			logger.error("Error in get subscription package id : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	/***
	 * CHANGE EXAM PAPER STATUS
	 */
	@CrossOrigin
	@GetMapping("/change-status/{id}")
	public Response changeStatus(@PathVariable("id") Long id) {
		Response response = new Response();
		try {
			return subscriptionPackageService.changeStatus(id);
		} catch (Exception e) {
			logger.error("Error in subscription package change status ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	/***
	 * DELETE SUBSCRIPTION
	 */
	@CrossOrigin
	@DeleteMapping("/delete/{id}")
	public Response delete(@PathVariable("id") Long id) {
		Response response = new Response();
		try {
			return subscriptionPackageService.delete(id);
		} catch (Exception e) {
			logger.error("Error in delete subscription package ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@GetMapping("/list")
	public Response list() {
		Response response = new Response();
		try {
			return subscriptionPackageService.list();
		} catch (Exception e) {
			logger.error("Error in subscription list ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@PostMapping("/web/list")
	public Response listActive(@RequestBody Filter filter) {
		Response response = new Response();
		try {
			return subscriptionPackageService.listActive(filter);
		} catch (Exception e) {
			logger.error("Error in list Active of subscription ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

}
