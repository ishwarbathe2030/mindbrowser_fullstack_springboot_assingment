package com.mindbrowser.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindbrowser.bean.Employee;
import com.mindbrowser.constants.Response;
import com.mindbrowser.constants.StatusConstance;
import com.mindbrowser.service.EmployeeService;

@RestController
@RequestMapping(value = "/v1/employee")
public class EmployeeController {

	private static final Logger logger = LogManager.getLogger(EmployeeController.class);

	private final EmployeeService employeeService;

	public EmployeeController(EmployeeService employeeService) {
		super();
		this.employeeService = employeeService;
	}

	@CrossOrigin
	@PostMapping("/add")
	public Response addnew(@RequestBody Employee emp) {
		Response response = new Response();
		try {
			return employeeService.addnew(emp);
		} catch (Exception e) {
			logger.error("Error In add  employee  : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@PutMapping("/update")
	public Response update(@RequestBody Employee emp) {
		Response response = new Response();
		try {
			return employeeService.update(emp);
		} catch (Exception e) {
			logger.error("Error In update employee : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	@CrossOrigin
	@GetMapping("/delete/{id}")
	public Response delete(@PathVariable("id") Long empId) {
		Response response = new Response();
		try {
			return employeeService.delete(empId);
		} catch (Exception e) {
			logger.error("Error in delete employee : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

	/****
	 * ALL EMPLOYEE LIST FOR MANAGER WISE
	 **************/
	@CrossOrigin
	@GetMapping("/employee-list/{managerId}")
	public Response empList(@PathVariable("managerId") Long managerId) {
		Response response = new Response();
		try {
			return employeeService.empList(managerId);
		} catch (Exception e) {
			logger.error("Error In employee list manager wise : ", e);
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			response.setResponse(e.getMessage());
			return response;
		}
	}

}
