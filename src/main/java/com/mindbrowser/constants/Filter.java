package com.mindbrowser.constants;

/**
 * @author ISHWAR BATHE
 * @since JUL 13, 2020
 * 
 */
public class Filter {

	private long page;

	private long size;

	private String searchBy;

	private String fromDate;

	private String toDate;

	private Long parentId;

	private Long userId;

	private boolean status;

	private int byNumStatus;

	private String subSearchBy;

	private Long categoryId;

	private Integer paymentType;

	private Integer paymentStatus;

	private String orderBy;
	
	private String subscription;

	public long getPage() {
		return page;
	}

	public void setPage(long page) {
		this.page = page;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getByNumStatus() {
		return byNumStatus;
	}

	public void setByNumStatus(int byNumStatus) {
		this.byNumStatus = byNumStatus;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Integer paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getSubSearchBy() {
		return subSearchBy;
	}

	public void setSubSearchBy(String subSearchBy) {
		this.subSearchBy = subSearchBy;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	
}
